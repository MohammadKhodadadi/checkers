from dataclasses import dataclass
from typing import List
from base import *
from piece import Piece, PiecesContainer, pieces_obj


@dataclass
class Rule:
    pieces: PiecesContainer
    new_role = None
    new_location = None
    array_board: dict = None

    def __post_init__(self):
        self.array_board = self.pieces.array_board

    def get_capture_pieces(self, piece: Piece, destination: tuple) -> list:
        raise NotImplementedError()

    def is_valid_move(self, piece: Piece, destination: tuple) -> bool:
        raise NotImplementedError()

    def reset_rule(self):
        self.new_role = None
        self.new_location = None

    def apply_rule(self, piece: Piece):
        if self.new_role is not None:
            piece.role = self.new_role
        if self.new_location is not None:
            piece.location = self.new_location


# Just for test
class King(Rule):
    def is_valid_move(self, piece: Piece, destination: tuple) -> bool:
        return False

    def get_capture_pieces(self, piece: Piece, destination: tuple) -> list:
        return []


# Just for test
class Man(Rule):
    def is_valid_move(self, piece: Piece, destination: tuple):
        return True

    def get_capture_pieces(self, piece: Piece, destination: tuple) -> list:
        return []


@dataclass
class RulesContainer:
    rules_cls: list
    pieces_obj: PiecesContainer
    rules_list: List[Rule] = None

    def __post_init__(self):
        self.rules_list = [kls(pieces_obj) for kls in self.rules_cls]


########################## CREATE OBJECT ######################################

rules_list = RulesContainer(rules_cls=[King, Man], pieces_obj=pieces_obj).rules_list

# if __name__ == '__main__':
#     import pprint
#     obj = RulesContainer(rules_cls=[King, Man], pieces_obj=pieces_obj).rules_list
#     pprint.pprint(obj)
