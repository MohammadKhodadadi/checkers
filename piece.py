from dataclasses import dataclass
from typing import List, Dict
from base import Base


@dataclass
class Piece:
    role: str
    location: tuple
    owner: int


@dataclass
class PiecesContainer(Base):
    array_board: dict = None
    pieces: List[Piece] = None

    def __post_init__(self):
        self.pieces = []
        for row in range(8):
            for col in range(8):
                if (row + col) % 2 == 0:
                    if row < 3:
                        # Create black pieces in rows 0-2
                        self.pieces.append(Piece(role='man', location=(row, col), owner=1))
                    elif row > 4:
                        # Create white pieces in rows 5-7
                        self.pieces.append(Piece(role='man', location=(row, col), owner=2))

        # Fill the array_board attribute with the locations of each piece
        self.fill_boards_array()

    def fill_boards_array(self) -> None:
        # Initialize the array_board with empty squares
        self.array_board = dict()

        # Populate the array_board with the positions of the pieces
        for piece in self.pieces:
            self.array_board[piece.location] = piece

    def print_board(self) -> None:
        # Initialize the board as a list of empty squares
        board = [['-' for _ in range(8)] for _ in range(8)]

        # Populate the board with the positions of the pieces
        for piece in self.pieces:
            row, col = piece.location
            if piece.owner == 1:
                # Black piece
                board[row][col] = 'B'
            else:
                # White piece
                board[row][col] = 'W'

        # Print the board to the console
        print('   A  B  C  D  E  F  G  H')
        print('  +----------------------+')
        for row in range(8):
            print(f'{row + 1} |', end='')
            for col in range(8):
                print(f' {board[row][col]}', end=' ')
            print(f'| {row + 1}')
        print('  +----------------------+')
        print('   A  B  C  D  E  F  G  H')


########################### CREATE OBJECT  #####################################
pieces_obj = PiecesContainer()


if __name__ == '__main__':
    import pprint

    obj = PiecesContainer()
    obj.print_board()
    pprint.pprint(obj.array_board)
    print(len(obj.array_board))
