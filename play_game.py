from dataclasses import dataclass
from board import Board
from piece import Piece
from game import Game
from rules import Rule, RulesContainer


@dataclass
class Play:
    board: Board
    rules: RulesContainer
    game: Game

    def __post_init__(self):
        pass