class Base:
    def __init__(self, array_board=None):
        self.array_board = array_board

    def fill_boards_array(self):
        raise NotImplementedError()

    def print_board(self):
        raise NotImplementedError()
