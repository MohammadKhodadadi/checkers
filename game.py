from dataclasses import dataclass
from typing import List
from .rules import RulesContainer, Rule
from .piece import Piece, pieces_obj
from .board import Board, board_obj
from .rules import rules_list, RulesContainer


@dataclass
class Game:
    counter = 0
    board: Board = board_obj
    rules: List[Rule] = rules_list
    current_piece: Piece | None = None

    def __post_init__(self):
        self.rules = self.board.rules

    def reset_rules(self):
        [x.reset_rule() for x in self.rules]
        self.current_piece = None

    def apply_rules(self):
        [rule.apply_rule(self.current_piece) for rule in self.rules]

    def _get_all_captures(self, piece_obj: Piece, destination: tuple):
        result = []
        for rule in self.rules:
            result.extend(rule.get_capture_pieces(piece_obj, destination))
        return result

    def move(self, source: tuple, destination: tuple):
        piece_obj = self.board.array_board.get(source)
        self.current_piece = piece_obj

        if (piece_obj is not None) and (piece_obj.owner == self.who_turn()):
            is_valid = any([rule.is_valid_move(piece_obj, destination) for rule in self.rules])

            if is_valid:
                all_capture_pieces = self._get_all_captures(piece_obj, destination)
                self.board.perform_move(source=piece_obj, destination=destination, captures=all_capture_pieces)

                # count the number of self.counter
                self.counter += 1
                # apply rules

                self.apply_rules()

                # reset rules
                self.reset_rules()

            else:
                raise PermissionError("This move is not allowed")

        else:
            raise PermissionError('you are not allowed to move this piece or you are not the owner of this piece')

    def who_turn(self) -> int:
        return self.counter % 2

    def show_board(self) -> None:
        # Show result based on self.board.array_board
        pass

################################ CREATE OBJECT  ########################################