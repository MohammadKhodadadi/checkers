from .rules import RulesContainer, Rule, rules_list
from .piece import Piece, PiecesContainer, pieces_obj
from typing import List
from dataclasses import dataclass


@dataclass
class Board:
    rules: List[Rule]
    pieces: PiecesContainer
    array_board: dict = None

    def __post_init__(self):
        self.array_board = self.pieces.array_board

    def perform_move(self, source: Piece, destination: tuple, captures: List[Piece]):
        # apply on all new changes on array_board
        pass


####################### CREATE OBJECT  #########################################
board_obj = Board(pieces=pieces_obj, rules=rules_list)
